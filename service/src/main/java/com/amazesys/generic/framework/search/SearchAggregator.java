package com.amazesys.generic.framework.search;


import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.search.model.SearchResponse;
import com.amazesys.generic.framework.search.supplier.SupplierService;
import com.amazesys.generic.framework.search.supplier.registry.SupplierRegistry;
import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;
import com.amazesys.generic.framework.supplier.model.SupplierRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class SearchAggregator {

    private ExecutorService executorService;

    @Resource
    private SupplierRegistry supplierRegistry;

    @Resource
    private Massager resultsMassager;

    @Value("${supplier.search.thread.pool.size:10}")
    private Integer threadPoolSize;

    @PostConstruct
    public void init(){
        executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    public SearchResponse execute(final SearchRequest request, List<SupplierMetadata> activeSuppliers)  {

        List<Future<List<SupplierRecord>>> futures = new ArrayList<>();
        SearchResponse searchResponse = new SearchResponse();

        for (final SupplierMetadata supplier : activeSuppliers){
            final SupplierService supplierService = supplierRegistry.get(supplier.getSupplierId());
            Future<List<SupplierRecord>> future = executorService.submit(() -> supplierService.executeWorkFlow(request, supplier));
            futures.add(future);
        }

        List<SupplierRecord> resultSupplierRecords = new ArrayList<>();
        for (Future<List<SupplierRecord>> future : futures){
            try {
                List<SupplierRecord> supplierRecords = future.get();
                resultSupplierRecords.addAll(supplierRecords);
            } catch (InterruptedException | ExecutionException e) {
                searchResponse.addErrorMsg("Error fetching records from supplier:" + e.getMessage());
            }
        }
        searchResponse.setData(resultsMassager.massage(resultSupplierRecords));
        return searchResponse;
    }

}
