package com.amazesys.generic.framework.search;


import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.search.model.SearchResponse;

import java.util.concurrent.ExecutionException;

public interface SearchService {

    SearchResponse search(SearchRequest request) throws ExecutionException, InterruptedException;
}
