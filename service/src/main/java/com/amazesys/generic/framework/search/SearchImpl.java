package com.amazesys.generic.framework.search;

import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.search.model.SearchResponse;
import com.amazesys.generic.framework.search.supplier.metadata.SupplierMetaService;
import com.amazesys.generic.framework.supplier.metadata.model.CompanySupplierMetadata;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@Component
public class SearchImpl implements SearchService {

    @Resource
    private SupplierMetaService supplierMetaService;

    @Resource
    private SearchAggregator searchAggregator;

    public SearchImpl(SupplierMetaService supplierMetaService, SearchAggregator searchAggregator) {
        this.supplierMetaService = supplierMetaService;
        this.searchAggregator = searchAggregator;
    }

    @Override
    public SearchResponse search(SearchRequest request) throws ExecutionException, InterruptedException {
        CompanySupplierMetadata companySupplierMetadata = supplierMetaService.fetch(request.getCompanyId());
        SearchResponse searchResponse = searchAggregator.execute(request, companySupplierMetadata.getSupplierMetadataList());
        return searchResponse;
    }
}
