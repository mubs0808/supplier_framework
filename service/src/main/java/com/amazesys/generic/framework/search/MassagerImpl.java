package com.amazesys.generic.framework.search;


import com.amazesys.generic.framework.search.cache.StaticContentCache;
import com.amazesys.generic.framework.supplier.model.SupplierRecord;
import com.amazesys.generic.framework.supplier.model.SupplierStaticData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MassagerImpl implements Massager{

    private StaticContentCache staticContentCache;

    public MassagerImpl(StaticContentCache staticContentCache) {
        this.staticContentCache = staticContentCache;
    }

    @Override
    public List<SupplierRecord> massage(List<SupplierRecord> inputRecords) {

        List<SupplierRecord> finalList = new ArrayList<>();

        //de-dup and compose, in parallel
        for (SupplierRecord inputRecord:inputRecords){
            SupplierStaticData supplierStaticData = staticContentCache.fetch(inputRecord.getSupplierId());
            compose(inputRecord, supplierStaticData);
            finalList.add(inputRecord);
        }
        return finalList;
    }

    private void compose(SupplierRecord inputRecord, SupplierStaticData supplierStaticData) {
        // compose record with static and dynamic
    }
}
