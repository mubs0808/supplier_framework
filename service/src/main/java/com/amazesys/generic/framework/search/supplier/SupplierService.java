package com.amazesys.generic.framework.search.supplier;

import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;
import com.amazesys.generic.framework.supplier.model.SupplierRecord;
import com.amazesys.generic.framework.supplier.model.SupplierRequest;
import com.amazesys.generic.framework.supplier.model.SupplierResponse;

import javax.xml.bind.ValidationException;
import java.util.List;

/**
 *  All the suppliers will have to implement this interface and define implementation of the methods
 *
 * @param <T>
 * @param <R>
 */
public interface SupplierService<T extends SupplierRequest, R extends SupplierResponse> {

    /**
     *
     *  This is the template method which does:
     *  *) Validation
     *  *) Request creation
     *  *) Repository invocation
     *  *) Response transformation
     *
     * @param request
     * @param supplierMetadata
     * @return
     * @throws ValidationException
     */
    //TODO: Think of using the 'supplierMetadata' from Cache
    default List<SupplierRecord> executeWorkFlow(SearchRequest request, SupplierMetadata supplierMetadata) throws ValidationException {

        List<SupplierRecord> records = null;
        if (validate(request)){
            T supplierRequest = buildRequest(request);
            R supplierResponse = invokeRepository(supplierRequest);
            return transformResponse(supplierResponse);
        }

        return records;
    }

    boolean validate(SearchRequest searchRequest) throws ValidationException;

    T buildRequest(SearchRequest searchRequest);

    R invokeRepository(T supplierRequest);

    List<SupplierRecord> transformResponse(R supplierResponse);

}
