package com.amazesys.generic.framework.search.supplier;


import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.supplier.model.SupplierRequest;

/**
 *
 *  Contract to build the Supplier specific request
 *
 * @param <T>
 */
public interface SupplierRequestBuilder<T extends SupplierRequest> {

    T build(SearchRequest searchRequest);
}
