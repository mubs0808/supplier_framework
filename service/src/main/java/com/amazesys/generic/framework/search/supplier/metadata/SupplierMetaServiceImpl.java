package com.amazesys.generic.framework.search.supplier.metadata;

import com.amazesys.generic.framework.search.supplier.metadata.dao.SupplierMetadataDao;
import com.amazesys.generic.framework.supplier.metadata.model.CompanySupplierMetadata;
import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


@Component
public class SupplierMetaServiceImpl implements SupplierMetaService {

    private Logger LOGGER = LoggerFactory.getLogger(SupplierMetaServiceImpl.class);

    @Resource
    private SupplierMetadataDao supplierMetadataDao;

    @Resource
    private Cache supplierMetadaCache;

    @Override
    public CompanySupplierMetadata fetch(String companyId) {

        CompanySupplierMetadata companySupplierMetadata = supplierMetadaCache.get(companyId, CompanySupplierMetadata.class);
        if (null == companySupplierMetadata){
            List<SupplierMetadata> activeSuppliers = supplierMetadataDao.getActiveSuppliers(companyId);
            companySupplierMetadata = new CompanySupplierMetadata(companyId, activeSuppliers);
            supplierMetadaCache.put(companyId, companySupplierMetadata);
            LOGGER.info("SupplierMetadata:{} added to cache !!");
        }
        return companySupplierMetadata;
    }
}
