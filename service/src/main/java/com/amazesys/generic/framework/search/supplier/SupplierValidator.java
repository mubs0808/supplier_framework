package com.amazesys.generic.framework.search.supplier;


import com.amazesys.generic.framework.search.model.SearchRequest;

/**
 *
 *  Contract to perform pre-validation steps before hitting the actual invoker
 *
 */
public interface SupplierValidator {

    boolean validate(SearchRequest searchRequest);

}
