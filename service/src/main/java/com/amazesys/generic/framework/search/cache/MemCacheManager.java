package com.amazesys.generic.framework.search.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import org.springframework.util.Assert;

import java.util.Collection;

/**
 */
public class MemCacheManager extends AbstractCacheManager{
    private final Collection<MemCache> internalCaches;

    public MemCacheManager(final Collection<MemCache> internalCaches) {
        this.internalCaches = internalCaches;
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        Assert.notNull(internalCaches, "A collection caches is required and cannot be empty");
        return internalCaches;
    }
}
