package com.amazesys.generic.framework.search.supplier.metadata;

import com.amazesys.generic.framework.supplier.metadata.model.CompanySupplierMetadata;

/**
 *
 *  Contract to fetch active supplier details for a company
 *
 */
public interface SupplierMetaService {

    /**
     *
     * @param companyId - input company id
     * @return Mapping of company to active suppliers
     */
    CompanySupplierMetadata fetch(String companyId);
}
