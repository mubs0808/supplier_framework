package com.amazesys.generic.framework.search;


import com.amazesys.generic.framework.supplier.model.SupplierRecord;

import java.util.List;

/**
 *  Modify/Massage the records as needed in the Result
 *
 */
public interface Massager {

    List<SupplierRecord> massage(List<SupplierRecord> inputRecords);
}
