package com.amazesys.generic.framework.search.supplier.hotelsdotcom;

import com.amazesys.generic.framework.search.model.SearchRequest;
import com.amazesys.generic.framework.search.supplier.SupplierService;
import com.amazesys.generic.framework.search.supplier.SupplierValidator;
import com.amazesys.generic.framework.supplier.model.SupplierRecord;

import javax.xml.bind.ValidationException;
import java.util.List;

/**
 *
 *  This is an example class showing how to use the Supplier Service framework
 *
 */
public class HotelsDotComService implements SupplierService<HotelsDotComRequest, HotelsDotComResponse> {

    private List<SupplierValidator> supplierValidators;
    private HotelsDotComRequestBuilder supplierRequestBuilder;
    private HotelsDotComResponseTransformer hotelsDotComResponseTransformer;


    @Override
    public boolean validate(SearchRequest searchRequest) throws ValidationException {
        for (SupplierValidator supplierValidator : supplierValidators){
            supplierValidator.validate(searchRequest);
        }
        return true;
    }

    @Override
    public HotelsDotComRequest buildRequest(SearchRequest searchRequest) {
        return supplierRequestBuilder.build(searchRequest);
    }

    @Override
    public HotelsDotComResponse invokeRepository(HotelsDotComRequest supplierRequest) {
        return null;
    }

    @Override
    public List<SupplierRecord> transformResponse(HotelsDotComResponse supplierResponse) {
        return hotelsDotComResponseTransformer.transform(supplierResponse);
    }
}
