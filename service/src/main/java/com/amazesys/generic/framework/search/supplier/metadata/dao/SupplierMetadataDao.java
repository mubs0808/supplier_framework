package com.amazesys.generic.framework.search.supplier.metadata.dao;


import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;

import java.util.List;

public interface SupplierMetadataDao {

    List<SupplierMetadata> getActiveSuppliers(String companyId);
}
