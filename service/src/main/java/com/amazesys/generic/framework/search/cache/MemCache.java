package com.amazesys.generic.framework.search.cache;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.transcoders.SerializingTranscoder;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;

/**
 *
 *  Cache used to store supplier metadata info
 */
public class MemCache implements Cache {

    private MemcachedClient cache;
    private String name;

    private static final Logger LOGGER = LoggerFactory.getLogger(MemCache.class);

    public MemCache(final String cacheName, final String addresses) throws URISyntaxException {
        this.name = cacheName;
        try {
            cache = new MemcachedClient(AddrUtil.getAddresses(addresses));
            final SerializingTranscoder stc = (SerializingTranscoder) cache.getTranscoder();
            stc.setCompressionThreshold(600000);
        } catch (final IOException e) { //Let it attempt to reconnect
            LOGGER.error("Error creating memcache client", e);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getNativeCache() {
        return cache;
    }

    @Override
    public ValueWrapper get(final Object key) {
        Object value = null;
        try {
            value = cache.get(key.toString());
        } catch (final Exception e) {
            LOGGER.warn("Exception fetching data from cache" + e);
        }
        if (value == null) {
            return null;
        }
        return new SimpleValueWrapper(value);
    }

    @Override
    public <T> T get(Object o, Class<T> aClass) {
        ValueWrapper valueWrapper = get(o);
        if (null != valueWrapper) {
            return aClass.cast(valueWrapper.get());
        }
        return null;
    }

    @Override
    public <T> T get(Object o, Callable<T> callable) {
        return null;
    }

    @Override
    public void put(final Object key, final Object value) {
        cache.set(key.toString(), 7 * 24 * 3600, value);
        Assert.assertNotNull(get(key)); //This fails on the viewCache
    }

    @Override
    public ValueWrapper putIfAbsent(Object o, Object o1) {
        return null;
    }

    @Override
    public void evict(final Object key) {
        this.cache.delete(key.toString());
    }

    @Override
    public void clear() {
        cache.flush();
    }
}
