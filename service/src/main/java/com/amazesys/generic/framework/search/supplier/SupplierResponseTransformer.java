package com.amazesys.generic.framework.search.supplier;


import com.amazesys.generic.framework.supplier.model.SupplierRecord;
import com.amazesys.generic.framework.supplier.model.SupplierResponse;

import java.util.List;

/**
 *
 *  Contract to transform the supplier specific response into a generic 'SupplierRecord'
 *
 * @param <T>
 */
public interface SupplierResponseTransformer<T extends SupplierResponse> {

    public List<SupplierRecord> transform(T supplierResponse);
}
