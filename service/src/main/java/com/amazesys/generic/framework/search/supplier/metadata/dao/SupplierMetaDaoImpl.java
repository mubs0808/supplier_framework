package com.amazesys.generic.framework.search.supplier.metadata.dao;

import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *  Adding dummy implementation for now. replace once ready.
 *
 */
@Component
public class SupplierMetaDaoImpl implements SupplierMetadataDao{

    @Override
    public List<SupplierMetadata> getActiveSuppliers(String companyId) {
        List<SupplierMetadata> supplierMetadataList = new ArrayList<>();
        SupplierMetadata supplierMetadata = new SupplierMetadata();
        supplierMetadata.setCompanyId(companyId);
        supplierMetadata.setSupplierName("Interhome");
        supplierMetadata.setSupplierId("B26A2C2B-93EC-4BF9-8447-B80C9D313B5A");
        supplierMetadata.setImplementationMethodName("Search");
        supplierMetadataList.add(supplierMetadata);
        return supplierMetadataList;
    }
}
