package com.amazesys.generic.framework.search.cache;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.DefaultKeyGenerator;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 *
 */
@Configuration
@EnableCaching
public class CacheConfig implements CachingConfigurer{

    @Value("${supplier.metadata.cache.server.addresses:localhost:11211}")
    private String cacheAddresses;

    @Value("${supplier.metadata.cache.name:supplierMetadataCache}")
    private String metadataCacheName;

    @Bean
    public Cache supplierMetadataCache(){
        return cacheManager().getCache(metadataCacheName);
    }

    @Override @Bean
    public CacheManager cacheManager() {
        CacheManager cacheManager;
        try {
            cacheManager = new MemCacheManager(internalCaches());
            return cacheManager;
        } catch (final URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CacheResolver cacheResolver() {
        return null;
    }

    @Bean
    public Collection<MemCache> internalCaches() throws URISyntaxException {
        final Collection<MemCache> caches = new ArrayList<MemCache>();
        caches.add(new MemCache(metadataCacheName, cacheAddresses));
        return caches;
    }

    @Override
    public KeyGenerator keyGenerator() {
        return new DefaultKeyGenerator();
    }

    @Override
    public CacheErrorHandler errorHandler() {
        return null;
    }
}
