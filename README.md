# SUUPLIER FRAMEWORK

### Background and Documentation

This is a generic framework being developed to collate information from different suppliers in parallel and provide search efficiency.
This project uses spring boot.

It has 3 core modules as of now:

Supplier-framework-model
Supplier-framework-service
Supplier-framework-web

More to come !!!!!