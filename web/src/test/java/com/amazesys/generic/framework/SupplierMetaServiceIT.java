package com.amazesys.generic.framework;


import com.amazesys.generic.framework.search.supplier.metadata.SupplierMetaService;
import com.amazesys.generic.framework.supplier.metadata.model.CompanySupplierMetadata;
import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SupplierMetaServiceIT extends SupplierITBase {

    @Resource
    private SupplierMetaService supplierMetaService;

    @Test
    public void testFetchSupplierMetadata(){
        CompanySupplierMetadata companyMetadata = supplierMetaService.fetch("anyCompanyId");
        assertNotNull(companyMetadata);
        assertTrue(!CollectionUtils.isEmpty(companyMetadata.getSupplierMetadataList()));
        SupplierMetadata supplierMetadata = companyMetadata.getSupplierMetadataList().get(0);
        assertEquals("Interhome", supplierMetadata.getSupplierName());
    }
}
