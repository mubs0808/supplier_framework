package com.amazesys.generic.framework;

import org.junit.Test;
import org.springframework.cache.Cache;

import javax.annotation.Resource;


public class MemcachedClientIT extends SupplierITBase{

    @Resource
    private Cache supplierMetadataCache;

    @Test
    public void testCaching(){

        supplierMetadataCache.put("testKey", "testValues");
        Cache.ValueWrapper valueWrapper = supplierMetadataCache.get("testKey");
        System.out.println("-----------" + valueWrapper.get());
    }

}
