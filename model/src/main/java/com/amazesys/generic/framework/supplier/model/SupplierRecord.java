package com.amazesys.generic.framework.supplier.model;

public class SupplierRecord {

    private String supplierId;

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }
}
