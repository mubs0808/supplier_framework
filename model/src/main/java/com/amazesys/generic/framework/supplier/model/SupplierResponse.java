package com.amazesys.generic.framework.supplier.model;

/**
 *  This can be a marker interface which is the base class for all supplier responses
 *
 */
public interface SupplierResponse {
}
