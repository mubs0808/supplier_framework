package com.amazesys.generic.framework.supplier.model;

/**
 *
 * This can be a marker interface which is base class for all the supplier Request classes
 *
 */
public interface SupplierRequest {
}
