package com.amazesys.generic.framework.supplier.metadata.model;


import java.io.Serializable;

public class SupplierMetadata implements Serializable{

    private String supplierId;
    private String companyId;
    private String supplierName;
    private String implementationTypeId;
    private String implementationMethodName;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getImplementationTypeId() {
        return implementationTypeId;
    }

    public void setImplementationTypeId(String implementationTypeId) {
        this.implementationTypeId = implementationTypeId;
    }

    public String getImplementationMethodName() {
        return implementationMethodName;
    }

    public void setImplementationMethodName(String implementationMethodName) {
        this.implementationMethodName = implementationMethodName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }
}
