package com.amazesys.generic.framework.search.model;


import com.amazesys.generic.framework.supplier.metadata.model.SupplierMetadata;

public class SearchContext {

    private SearchRequest inputRequest;

    private SupplierMetadata supplierMetadata;

    public SearchContext(SearchRequest inputRequest, SupplierMetadata supplierMetadata) {
        this.inputRequest = inputRequest;
        this.supplierMetadata = supplierMetadata;
    }

    public SupplierMetadata getSupplierMetadata() {
        return supplierMetadata;
    }

    public void setSupplierMetadata(SupplierMetadata supplierMetadata) {
        this.supplierMetadata = supplierMetadata;
    }

    public SearchRequest getInputRequest() {
        return inputRequest;
    }

    public void setInputRequest(SearchRequest inputRequest) {
        this.inputRequest = inputRequest;
    }
}
