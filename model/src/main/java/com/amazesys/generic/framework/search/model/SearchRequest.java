package com.amazesys.generic.framework.search.model;


import java.util.Date;

public class SearchRequest {

    private String destination;

    private Date fromDate;

    private Date toDate;

    private Enum SearchType;

    private String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Enum getSearchType() {
        return SearchType;
    }

    public void setSearchType(Enum searchType) {
        SearchType = searchType;
    }
}
