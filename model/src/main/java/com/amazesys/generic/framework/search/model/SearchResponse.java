package com.amazesys.generic.framework.search.model;


import com.amazesys.generic.framework.supplier.model.SupplierRecord;

import java.util.ArrayList;
import java.util.List;

public class SearchResponse {

    private List<SupplierRecord> data;

    private List<String> errorMessages;

    public SearchResponse() {
        this.data = new ArrayList<SupplierRecord>();
        this.errorMessages = new ArrayList<>();
    }

    public List<SupplierRecord> getData() {
        return data;
    }

    public void addRecords(List<SupplierRecord> records){
        data.addAll(records);
    }

    public void setData(List<SupplierRecord> data) {
        this.data = data;
    }

    public void addErrorMsg(String message){
        errorMessages.add(message);
    }
}
