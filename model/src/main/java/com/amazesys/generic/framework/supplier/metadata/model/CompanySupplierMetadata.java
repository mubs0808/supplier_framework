package com.amazesys.generic.framework.supplier.metadata.model;

import java.io.Serializable;
import java.util.List;

/**
 */
public class CompanySupplierMetadata implements Serializable{

    private String companyId;

    private List<SupplierMetadata> supplierMetadataList;

    public CompanySupplierMetadata(String companyId, List<SupplierMetadata> supplierMetadataList) {
        this.companyId = companyId;
        this.supplierMetadataList = supplierMetadataList;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<SupplierMetadata> getSupplierMetadataList() {
        return supplierMetadataList;
    }

    public void setSupplierMetadataList(List<SupplierMetadata> supplierMetadataList) {
        this.supplierMetadataList = supplierMetadataList;
    }
}
